import { derived, get } from 'svelte/store';
import { getUserCollections } from "../lib/user/collections.js"
import { state, articles, collectionArticles, collectionList, feeds, clusters } from "./stores.js"
import { appConfig } from "./config.js"

export const allArticlesRead = derived([currentArticles, collectionList], async ([$currentArticles, $collectionList]) => {
	let articleList = await $currentArticles

	if (!$collectionList?.["Alread Read"] || !Boolean(articleList)) {
		return false
	}

	if (Boolean(articleList)) {
		for (const article of articleList) {
			if (!$collectionList?.["Already Read"]?.includes?.(article.id)) {
				return false
			}
		}

		return true
	} else {
		return false
	}
})
