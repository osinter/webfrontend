import { writable } from 'svelte/store';
import { appConfig } from "./config.js"
import { ArticleSetsStore, CurrentArticleSet, CurrentArticles } from "../lib/objects/groups.js"

export const state = writable(structuredClone(appConfig.defaultOptions.state))
export const modalState = writable(structuredClone(appConfig.defaultOptions.modalState))
export const loginState = writable(structuredClone(appConfig.defaultOptions.loginState))

export const currentSearch = writable({})

export const articleSets = new ArticleSetsStore()
export const currentArticleSet = new CurrentArticleSet(articleSets)
export const currentArticles = new CurrentArticles(articleSets, currentArticleSet)
