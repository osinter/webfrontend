export const appConfig = {
	"version" : 4,
	"rootUrl" : "/api",
	"refreshRate" : 5, // Minimum time in minutes between refreshing of article lists
	"permCollections" : ["Read Later", "Already Read"],
	"defaultOptions" : {
		"search" : {
			"source_category" : [],
			"sort_by" : "publish_date",
			"sort_order" : "desc",
			"first_date" : null,
			"last_date" : null,
			"search_term" : null,
			"limit" : 100,
			"highlight" : false
		},
		"state" : {
				"selectedMenu" : {
					"name" : "News",
					"type" : "feed",
				},
				"representation" : "Large",
				"localSearch" : "",
				"sidebar" : {
					"available" : true,
					"closed" : {
						"feeds" : false,
						"collections" : false,
						"user" : false,
						"about" : true
					}
				},
				"availability": {
					"ml" : false
				}
		},
		"modalState" : {
				"modalType" : null,
				"modalContent" : null
		},
		"loginState" : {
				"loggedIn" : false,
				"userObject" : {}
		},
		"modalStates" : {
			"signup" : {
				"modalType" : "auth",
				"modalContent" : {
					"type" : "signup",
					"title" : "Hi There!",
					"desc" : "Sign up below to start your own journey into the wonderful world of CTI"
				}
			},
			"login" : {
				"modalType" : "auth",
				"modalContent" : {
					"type" : "login",
					"title" : "Welcome back!",
					"desc" : "Log in down below to continue with your journey into the wonderful world of CTI"
				}
			}
		}
	},
	"userOptions" : {
		"loggedIn" : {
			"Read Later" : { "icon" : "bookmarks", "type" : "collection"},
			"Already Read" : { "icon" : "bookmarks", "type" : "collection"}//,
			//"Configure Profile" : { "icon" : "gear", "type" : "userOptions" }
		}
	},
	"feeds" : {
		"mainFeeds" : {
			"Today" : {
				"last_date" : (new Date()).toISOString(),
				"first_date" : (new Date(new Date().getTime() - (24 * 60 * 60 * 1000))).toISOString()
			},
			"News" : { }
		},
		"userFeeds" : {
			"Log4J" : {
				"sort_by" : "publish_date",
				"sort_order" : "desc",
				"search_term" : "log4j",
				"highlight" : true
			},
			"Bitcoin" : {
				"search_term" : "bitcoin",
				"highlight" : true
			},
			"Ransomware" : {
				"sort_by" : "publish_date",
				"sort_order" : "desc",
				"search_term" : "ransomware",
				"highlight" : true
			}
		}
	}
}

const deepFreeze = (obj) => {
	Object.keys(obj).forEach((property) => {
		if (
			typeof obj[property] === "object" &&
			!Object.isFrozen(obj[property])
		)
		deepFreeze(obj[property]);
	});
	return Object.freeze(obj);
};

deepFreeze(appConfig)
