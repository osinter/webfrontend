import { modalState, state, loginState, articleSets } from "../shared/stores.js"
import { appConfig } from "../shared/config.js"

import { queryAPI } from "./common.js"

import { get } from "svelte/store"

export function exploreCluster(clusterID) {
	const clusterName = articleSets.addCluster(clusterID)
	modalState.set({"modalType" : null, "modalContent" : null})
	state.update(currentState => ({...currentState, "selectedMenu" : {"name" : clusterName, "type" : "cluster"}, "localSearch" : ""}))
}

export async function moveClusterToCollection(cluster) {
	const articleIDs = (await this.articles).map(article => article.id)

	const callback = (setName) => {
		modalState.set(appConfig.defaultOptions.modalState)
		state.update(currentState => ({...currentState, "selectedMenu" : {"name" : setName, "type" : "collection"}, "localSearch" : ""}))
		articleSets.removeCluster(cluster.name)
	}

	createSet("collection", callback, articleIDs, `Cluster ${cluster.name}`)
}

export function showSearchModal() {
	modalState.set({"modalType" : "search", "modalContent" : null})
	document.activeElement.blur()
}

export async function createArticleModal(articleID) {
	let articleContent = (await queryAPI(`/articles/content?ids=${articleID}`, [{}]))[0]
	modalState.set({"modalType" : "article", "modalContent" : articleContent})
	articleSets.modifyCollection("Already Read", "extend", articleID)
}

export function search(feed) {
	if (!feed) return

	articleSets.search(feed)

	state.update(currentState => {
		currentState.selectedMenu = {"name" : "Custom search", "type" : "feed"}
		currentState.localSearch = ""
		return currentState
	})

	modalState.set({ "modalType" : null, "modalContent" : null })
}

const storeOverview = {
	"state" : {"store" : state, "defaultValue" : structuredClone(appConfig.defaultOptions.state), "cacheAble" : true},
	"modalState" : {"store" : modalState, "defaultValue" : structuredClone(appConfig.defaultOptions.modalState), "cacheAble" : true},
	"loginState" : {"store" : loginState, "defaultValue" : structuredClone(appConfig.defaultOptions.loginState), "cacheAble" : false},
}

export async function resetState() {
	for (const storeDetails of Object.values(storeOverview)) {
		storeDetails.store.set(storeDetails.defaultValue)
	}

	articleSets.reset()
}

export async function syncStateToLocalStorage() {
	let unsubscribeFunctions = []

	Object.keys(storeOverview).forEach((s) => {
		if (!storeOverview[s].cacheAble) return

			unsubscribeFunctions.push(
				storeOverview[s].store.subscribe((value) => localStorage.setItem(s, JSON.stringify(value)))
			)
	})

	return () => { unsubscribeFunctions.forEach(funcName => funcName())}
}

export async function syncLocalStorageToState() {
	if (localStorage.getItem("frontend-version") != appConfig.version) {
		localStorage.setItem("frontend-version", appConfig.version)
		return
	}

	Object.keys(storeOverview).forEach((s) => {
		if (!storeOverview[s].cacheAble) return

		const currentValue = localStorage.getItem(s)

		if (Boolean(currentValue)) {
			try {
				storeOverview[s].store.set(JSON.parse(currentValue))
			} catch (e) {
				console.log(`Failed to read state details regarding "${s}", with the following value and error. Clearing it for now.`, currentValue, e)
				localStorage.removeItem(s)
			}
		}
	})
}
