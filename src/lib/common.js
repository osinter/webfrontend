import { appConfig } from "../shared/config.js";
import { loginState } from "../shared/stores.js"

export function compare(v1, v2) {
	if (!v1 && !v2) return true

	if (Array.isArray(v1)) {
		return v1.sort().join("") === v2?.sort()?.join("")
	} else if (v1 instanceof Date) {
		return v1.getTime() === v2.getTime()
	} else if ([v1, v2].every(v => Object.prototype.toString.call(v) === "[object Object]")) {
		return Object.entries(v1).every(([k, v]) => compare(v, v2[k])) && Object.keys(v1).length === Object.keys(v2).length
	} else {
		return v1 === v2
	}
}

const epochs = [
    ['year', 31536000],
    ['month', 2592000],
    ['day', 86400],
    ['hour', 3600],
    ['minute', 60],
    ['second', 1]
]

export function getTimespan(origDate) {
	let timeSpan = (Date.now() - Date.parse(origDate)) / 1000
	for (let [name, seconds] of epochs) {
		if (timeSpan > seconds) {
			let timeAmount = Math.floor(timeSpan / seconds)
			return `${timeAmount} ${name}${timeAmount > 1 ? "s" : ""} ago`
		}
	}
}

export async function queryAPI(queryURL, defaultResponse = null) {
	let queryResult = await fetch(`${appConfig.rootUrl}${queryURL}`)

	if (queryResult.ok) {
		return await queryResult.json()
	} else {
		return defaultResponse
	}
}

export async function queryProtected(queryURL, method = "GET", body = null) {
	let headers

	headers = {
		credentials : "include",
		method : method,
		headers: {'Content-Type': 'application/json'},
		body : body ? JSON.stringify(body) : null
	}

	let queryResult = await fetch(`${appConfig.rootUrl}${queryURL}`, headers)

	if (queryResult.status == 401) {
		return {"status" : "not-auth", "content" : "User needs to be logged in to access this feature"}
	} else {
		loginState.update(currentState => {
			currentState.loggedIn = true
			return currentState
		})

		try {
			if (queryResult.ok) {
				return {"status" : "success", "content" : await queryResult.json()}
			} else {
				console.log("Error in query: ", queryResult)
				return {"status" : "failure", "content" : (await queryResult.json())["detail"] }
			}
		} catch {
			return {"status" : "failure", "content" : "An unexpected error occured, please try again"}
		}
	}
}
