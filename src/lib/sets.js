import { get } from 'svelte/store';

import { appConfig } from "../shared/config.js"

import { state, modalState, loginState, articleSets, currentArticleSet } from "../shared/stores.js";
import { showSearchModal } from "./state.js"

export async function createSet(setType, callback = null, contents = null, nameSuggestion = null) {
	const setGroups = { "collection" : "collections", "feed" : "userFeeds" }
	await articleSets.autoUpdateGroup(setGroups[setType])

	if (get(loginState).loggedIn) {
			callback = callback ?? ((setName) => {
				modalState.set(appConfig.defaultOptions.modalState)
				state.update(currentState => ({...currentState, "selectedMenu" : {"name" : setName, "type" : setType}, "localSearch" : ""}))
			})

			modalState.set({
					"modalType" : "getName",
					"modalContent" : {
									"contentType" : setType,
									"content" : nameSuggestion,
									"action" : async (setName) => articleSets.createSet(setName, setType, contents, callback),
									"existingNames" : articleSets.getSetNames(setType)
						}
				})
	} else {
		modalState.set({"modalType" : "auth", "modalContent" : {"type" : "login", "title" : "Login here", "desc" : `Login here or signup with the link down below to create ${setType}s.`}})
	}
}

export const addCollection = async () => createSet("collection")

let currentState
state.subscribe((newState) => currentState = newState)

export async function addFeed() {
	if(currentState.selectedMenu.type !== "feed" || !(currentState.selectedMenu.name in articleSets.contents.search)) {
		showSearchModal()
	} else {
		await createSet("feed", null, articleSets.contents.search["Custom search"].sourceSpecs)
	}
}
