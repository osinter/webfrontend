import { compare, queryAPI, queryProtected } from "../common.js"

import { appConfig } from "../../shared/config.js"
import { state } from "../../shared/stores.js"
import { FeedSet, CollectionSet, ClusterSet } from "./primitives.js"

import { get } from "svelte/store"

class ArticleSetsGroup {
	constructor() {
		this.settings = {
			"permFeeds" : {"modifiable" : false, "objectClass" : FeedSet, "type" : "feed"},
			"preFeeds" : {"modifiable" : false, "objectClass" : FeedSet, "type" : "feed"},
			"userFeeds" : {"modifiable" : {"remove" : true}, "objectClass" : FeedSet, "type" : "feed", "updateUrl" : "/users/feeds"},

			"search" : {"modifiable" : false, "objectClass" : FeedSet, "type" : "feed"},

			"collections" : {"modifiable" : {"remove" : true}, "objectClass" : CollectionSet, "type" : "collection", "updateUrl" : "/users/collections"},
			"clusters" : {"modifiable" : {"add" : true}, "objectClass" : ClusterSet, "type" : "cluster"},
		}

		this.contents = {}
		this.reset()
	}

	reset() {
		this.contents = {
			"permFeeds" : {},
			"preFeeds" : {},
			"userFeeds" : {},

			"search" : {},

			"collections" : {},
			"clusters" : {}
		}

		this.populatePermanent()
	}


	populatePermanent() {
		this.updateSingleGroup("permFeeds", appConfig.feeds.mainFeeds)
		this.contents.permFeeds.Today.hideRead = true

		this.updateSingleGroup("preFeeds", appConfig.feeds.userFeeds)
	}

	async updateGroups() {
		if ( (await queryProtected("/auth/status"))["status"] != "not-auth" ) {
			Object.keys(this.contents)
				.filter(groupName => this.settings[groupName].updateUrl)
				.forEach(name => this.autoUpdateGroup(name))
		}
	}

	autoUpdateGroup(groupName) {
		let url = this.settings[groupName].updateUrl

		if (!url) {
			console.log(`Error when updating group ${groupName}, no update URL available`)
			return false
		}

		queryAPI(url + "/list").then(response => {
			if (response) this.updateSingleGroup(groupName, response)
			else console.log(`Error when updating group ${groupName} with url "${url}`)
		})
	}

	getSet(type, name) {
		const groups = Object.entries(this.settings)
							.filter(([name, setting]) => setting.type === type)
							.map(([name, setting]) => name)

		for (const groupName of groups) {
			if (name in this.contents[groupName]) {
				return this.contents[groupName][name]
			}
		}
	}

	getSetNames(type) {
		return Object.entries(this.settings)
						.filter(([name, setting]) => setting.type === type)
						.map(([name, setting]) => Object.keys(this.contents[name]))
						.reduce((accumulator, currentValue) => accumulator.concat(currentValue))
	}

	removeSet(setName, type, callback = null) {
		const settings = Object.entries(this.settings)
							.filter(([groupName, setting]) => setting.type == type
															&& setting.updateUrl
															&& setName in this.contents[groupName])
							.map(([groupName, setting]) => ({ ...setting, "name" : groupName }))[0]

		const url = `${settings.updateUrl}/${encodeURIComponent(setName)}`
		this.syncWithOnlineState(settings.name, setName, url, "DELETE", null, callback)
	}

	createSet(setName, type, contents = null, callback = null) {
		const settings = Object.entries(this.settings)
							.filter(([groupName, setting]) => setting.type == type && setting.updateUrl)
							.map(([groupName, setting]) => ({ ...setting, "name" : groupName }))[0]

		const url = `${settings.updateUrl}/${encodeURIComponent(setName)}`
		this.syncWithOnlineState(settings.name, setName, url, "PUT", contents, callback)
	}

	modifyCollection(collectionName, action, ids = null, callback = null) {
		let url = `${this.settings.collections.updateUrl}/${encodeURIComponent(collectionName)}/${encodeURIComponent(action)}`

		if (Array.isArray(ids)) {
			url += "?ids=" + ids.map(id => encodeURIComponent(id)).join("&ids=")
		} else if (ids) {
			url += `?ids=${encodeURIComponent(ids)}`
		}

		this.syncWithOnlineState("collections", collectionName, url, "POST", null, callback)
	}

	addCluster(clusterID, clusterName = null) {
		const cluster = new this.settings.clusters.objectClass(clusterName, clusterID)
		this.contents.clusters = { ...this.contents.clusters, [cluster.name] : cluster }
		this.refresh("clusters")
		return cluster.name
	}

	removeCluster(name) {
		delete this.contents.clusters[name]
		this.refresh("clusters")
	}

	search(feed, feedName = "Custom search") {
		const specs = feed.sourceSpecs ?? feed
		this.contents.search[feedName] = new this.settings.search.objectClass(feedName, specs, true)
		this.refresh("search")
	}


	syncWithOnlineState(groupName, setName, url, method, contents = null, callback = null) {
		queryProtected(url, method, contents).then((r) => {
			if (r.status === "success") {
				this.updateSingleGroup(groupName, r.content)
				if (callback) callback(setName)
			} else {
				console.log(`Failed to "${method}" "${setName}". Error:`, r.content)
			}
		})
	}

	updateSingleGroup(groupName, newSets) {
		const currentGroup = this.contents[groupName]
		const settings = this.settings[groupName]

		const newGroup = {}
		const modifiedSets = []

		for (const [setName, setSpecs] of Object.entries(newSets)) {
			if (setName in currentGroup) {
				newGroup[setName] = currentGroup[setName]

				if (!compare(setSpecs, currentGroup[setName].sourceSpecs)) {
					newGroup[setName].sourceSpecs = setSpecs
					modifiedSets.push(setName)
				}
			} else {
				newGroup[setName] = new settings.objectClass(setName, setSpecs, settings.modifiable)
				modifiedSets.push(setName)
			}

		}
		this.contents[groupName] = newGroup
		this.refresh(groupName)
	}

	refresh(groupName) {
		console.log(`Updating ${groupName}`)
	}
}

export class ArticleSetsStore extends ArticleSetsGroup {
	constructor() {
		super()
		this.subscribers = []

		this.refresh = (groupName = "") => {
			super.refresh(groupName)
			this.subscribers.forEach((s) => s(this.contents))
		}
	}


	subscribe(listener) {
		this.subscribers.push(listener)

		listener(this.contents)

		return () => {
			const index = this.subscribers.indexOf(listener)
			if (index !== -1) {
				this.subscribers.splice(index, 1)
			}
		}
	}
}

// Store derived from an instance of ArticleSetsStore
export class CurrentArticleSet {
	constructor(setStore) {

		this._selected = {
			"name" : "",
			"type" : ""
		}

		this.selectedSet = null

		this.setStore = setStore
		this.subscribers = []

		state.subscribe(currentState => {
			if (!compare(currentState.selectedMenu, this.selected)) {
				this.selected = currentState.selectedMenu
			}
		})

		setStore.subscribe(currentSets => {
			const newSet = this.getCurrentlySelected()

			// Will catch when a set gets removed by the user
			if (!newSet) {
				this.resetSelected()
				return
			}

			// Will update the selected set in case it has been changed out for another set
			if (!compare(this.setStore.sourceSpecs, newSet.sourceSpecs)) {
				this.selectedSet = newSet
				this.notifySubscribers()
				return
			}
		})

	}

	subscribe(listener) {
		this.subscribers.push(listener)

		listener(this.selectedSet)

		return () => {
			const index = this.subscribers.indexOf(listener)
			if (index !== -1) {
				this.subscribers.splice(index, 1)
			}
		}
	}

	notifySubscribers() {
		this.subscribers.forEach((s) => s(this.selectedSet))
	}

	set selected(selected) {
		this._selected = selected
		this.updateSelectedArticles()
	}

	get selected() {
		return this._selected
	}

	updateSelectedArticles() {
		const newSet = this.getCurrentlySelected()

		// Will catch when the input is wrong
		if (!newSet) {
			this.resetSelected()
			return
		}

		this.selectedSet = newSet
		this.notifySubscribers()
	}

	getCurrentlySelected() {
		return this.setStore.getSet(this.selected.type, this.selected.name)
	}

	resetSelected () {
		state.update(currentState => ({ ...currentState, "selectedMenu" : appConfig.defaultOptions.state.selectedMenu}) )
	}
}

// Store derived from an instance of CurrentArticleSet
export class CurrentArticles {
	constructor(groupStore, setStore) {

		this._search = ""
		this._currentSet = null

		this._selectedArticles = []

		this._hideRead = false

		this.groupStore = groupStore
		this.setStore = setStore

		this.subscribers = []

		this.currentSetUnsubscribe = () => {}

		state.subscribe(currentState => {
			if (!compare(currentState.localSearch, this.search)) {
				this.search = currentState.localSearch
			}
		})

		setStore.subscribe(currentSet => {
			this.currentSet = currentSet	
		})

	}

	get hideRead() {
		return this.currentSet.hideRead
	}

	set hideRead(hide) {
		this.currentSet.hideRead = hide
		this.selectedArticles = this.getSelectedArticles()
	}

	set currentSet(set) {
		this._currentSet = set

		this.currentSetUnsubscribe()
		this.currentSetUnsubscribe = set.subscribe((currentSet) => {
			this.selectedArticles = currentSet._articles?.then((articles) => {
				return this.getSearchedArticles(articles)
			})
		})

		if (set.refreshNeeded) set.update()
	}

	get currentSet() {
		return this._currentSet
	}


	set search(search) {
		this._search = search
		this.selectedArticles = this.getSelectedArticles()
	}

	get search() {
		return this._search
	}


	set selectedArticles(articles) {
		this._selectedArticles = articles
		this.subscribers.forEach((s) => s(this.selectedArticles))
	}

	get selectedArticles() {
		return this._selectedArticles
	}


	async getSelectedArticles(articles = null) {
		if (!articles) {
			articles = await this.currentSet.articles
		}

		return this.getSearchedArticles(articles)
	}


	getSearchedArticles(articles) {
		if (this.search) {
			articles = articles.filter(article => Object.values(article)
						.some(articleField => articleField.toLowerCase()
						.includes(this.search.toLowerCase())))
		}

		if (this.currentSet.hideRead && this.groupStore.contents.collections?.["Already Read"]) {
			articles = articles.filter(article => !this.groupStore.contents.collections["Already Read"].includes(article.id))
		}

		return articles
	}

	subscribe(listener) {
		this.subscribers.push(listener)

		listener(this.selectedArticles)

		return () => {
			const index = this.subscribers.indexOf(listener)
			if (index !== -1) {
				this.subscribers.splice(index, 1)
			}
		}
	}
}
