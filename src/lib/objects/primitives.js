import { appConfig } from "../../shared/config.js";

// For fetch data for respectively the FeedContents, CollectionContents and ClusterContents object
import { queryProtected } from "../common.js"
import { queryAPI } from "../common.js"

const FEED_DEFAULTS = appConfig.defaultOptions.search
const USED_SEARCH_PARAMS = ["sort_by", "sort_order", "search_term", "source_category", "limit", "highlight", "first_date", "last_date"]

export class FeedSpecs {
	constructor({
			limit = FEED_DEFAULTS.limit,
			sort_by = FEED_DEFAULTS.sort_by,
			sort_order = FEED_DEFAULTS.sort_order,
			search_term = FEED_DEFAULTS.search_term,
			first_date = FEED_DEFAULTS.first_date,
			last_date = FEED_DEFAULTS.last_date,
			source_category = FEED_DEFAULTS.source_category,
			highlight = FEED_DEFAULTS.highlight
		} = {}
	) {
		this.limit = limit
		this.sort_by = sort_by
		this.sort_order = sort_order
		this.search_term = search_term
		this.highlight = highlight

		this.source_category = source_category ? source_category : []
		this.first_date = first_date
		this.last_date = last_date
	}

	get first_date() {
		return this._first_date?.toISOString()
	}

	get last_date() {
		return this._last_date?.toISOString()
	}

	set first_date(date) {
		this._first_date = date ? new Date(date) : null
	}

	set last_date(date) {
		this._last_date = date ? new Date(date) : null
	}

	get queryParams() {
		// Remove values that are null or empty using filter, as those can be problematic when calling the API
		const query = {}

		USED_SEARCH_PARAMS.forEach(param => {
			const value = this[param]

			if (value && (!Array.isArray(value) || value.length > 0)) {
				query[param] = value
			}
		})


		return query
	}

	get queryUrl() {
		const queryParams = this.queryParams
		
		return Object.keys(queryParams)
			.filter(key => queryParams[key])
			.map(key => `${key}=` + (	Array.isArray(queryParams[key])
										? queryParams[key].map(value => encodeURIComponent(value)).join(`&${key}=`)
										: encodeURIComponent(queryParams[key])
									)
			).join('&');
	}

	get downloadLink() {
		return `${appConfig.rootUrl}/articles/export/search?${this.queryUrl}`
	}

	reset() {
		USED_SEARCH_PARAMS.forEach(param => {
			this[param] = FEED_DEFAULTS[param]
		})
	}

	static get defaults() {
		return Object.fromEntries(USED_SEARCH_PARAMS.map(param => [param, FEED_DEFAULTS[param]]))
	}
}

export class ArticleSet {
	constructor(name, icon, modifiable, type) {
		this.name = name
		this.icon = icon
		this.modifiable = modifiable
		this.type = type

		this.lastUpdateTime = Date.now();

		this.subscribers = []
	}

	subscribe(listener) {
		this.subscribers.push(listener)

		listener(this)

		return () => {
			const index = this.subscribers.indexOf(listener)
			if (index !== -1) {
				this.subscribers.splice(index, 1)
			}
		}
	}

	get refreshNeeded() {
		return (Date.now() - this.lastUpdateTime) / 60000 > appConfig.refreshRate;
	}

	refresh() {
		this.lastUpdateTime = Date.now()

		if (this.subscribers.length > 0) {
			this.subscribers.forEach((s) => s(this));
		}
	}
}

export class FeedSet extends ArticleSet {
	constructor(name, feed, modifiable, icon = "text-paragraph") {
		super(name, icon, modifiable, "feed");

		if (feed instanceof FeedSpecs) this.feed = feed;
		else this.sourceSpecs = feed;
	}

	get feed() {
		return this._feed
	}

	set feed(feed) {
		this._feed = feed
		this.update()
	}

	get sourceSpecs() {
		return this._feed.queryParams
	}

	set sourceSpecs(feed) {
		this._feed = new FeedSpecs(feed)
		this.update()
	}


	get articles() {
		if (this.refreshNeeded) this.update()
		return this._articles
	}

	set articles(articles) {
		this._articles = articles
	}


	get downloadLink() {
		return this._feed?.downloadLink
	}


	update() {
		this.articles = queryAPI(`/articles/search?${this.feed.queryUrl}`);
		this.refresh()
	}
}

export class LazyArticleSet extends ArticleSet {
	constructor(name, icon, modifiable, sourceSpecs, type) {
		super(name, icon, modifiable, type)
		this.sourceSpecs = sourceSpecs
	}

	refresh() {
		super.refresh()
		this.sourceSpecsChanged = false
	}

	get refreshNeeded() {
		return this.sourceSpecsChanged || super.refreshNeeded
	}

	get sourceSpecs() {
		return this._sourceSpecs
	}

	set sourceSpecs(specs) {
		this.sourceSpecsChanged = true
		this._sourceSpecs = specs
	}

	get articles() {
		if (this.refreshNeeded) this.update()

		return this._articles
	}

	set articles(articles) {
		this._articles = articles
	}

}

export class CollectionSet extends LazyArticleSet {
	constructor(name, ids, modifiable = {"remove" : true}) {
		super(name, "collection", modifiable, ids, "collection")
	}

	get downloadLink() {
		return `${appConfig.rootUrl}/users/collections/${encodeURIComponent(this.name)}/export`
	}

	update() {
		this.articles = ( async () => {
			const response = await queryProtected(`/users/collections/${this.name}`);

			if (response.status === "success") {
				return response.content
			} else {
				console.log(`Failed to get "${this.name}" collection, with following error: `, response.content)
				return
			}
		}) ()

		this.refresh()
	}

	includes(id) {
		return this.sourceSpecs.includes(id)
	}
}

export class ClusterSet extends LazyArticleSet {
	constructor(name, clusterID, modifiable = {"add" : true}) {
		super(name ?? `Cluster ${clusterID}`, "diagram-3", modifiable, clusterID, "cluster")
	}

	update(clusterID = null) {
		this.articles = queryAPI(`/ml/articles/cluster/${this.sourceSpecs}`)
		this.refresh()
	}

	get downloadLink() {
		return `${appConfig.rootUrl}/ml/articles/cluster/${encodeURIComponent(this.sourceSpecs)}/export`
	}
}
